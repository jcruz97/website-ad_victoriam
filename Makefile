include .env

update:
	$(call exec, "composer install --no-suggest")
	$(call exec, "vendor/bin/drush cr")
	$(call exec, "vendor/bin/drush updatedb -y")
	$(call exec, "vendor/bin/drush cim -y")
	$(call exec, "vendor/bin/drush sset system.maintenance_mode 0")
	$(call exec, "vendor/bin/drush cr")

rebuild:
	docker-compose up -d --build --force-recreate

cc:
	$(call exec, "vendor/bin/drush cr")

cex:
	$(call exec, "vendor/bin/drush cex")

cim:
	$(call exec, "vendor/bin/drush cim")

front:
	yarn && yarn build

composer:
	$(call exec, "composer ${COMMAND}")

ssh:
	docker exec -it ${APP_NAME}_php bash

drush:
	$(call exec, "vendor/bin/drush ${COMMAND}")

define exec
	docker exec -w /app/drupal -it ${APP_NAME}_php bash -c ${1}
endef